/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package xoprogram;

import java.util.Scanner;

/**
 *
 * @author Lenovo
 */
public class XOProgram {

    static char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'O';
    static int row, col;
    static Scanner sc = new Scanner(System.in);
    static boolean finish = false;
    static int count=0;

    public static void main(String[] args) {

        showWelcome();
        while (true) {
            showTable();
            showTurn();
            inputRowCol();
            process();
            if (finish) {
                break;
            }
        }

    }

    public static void showTable() {
        for (int r = 0; r < table.length; r++) {
            for (int c = 0; c < table[r].length; c++) {
                System.out.print(table[r][c]);
            }
            System.out.println("");
        }
    }

    public static void showWelcome() {
        System.out.println("Welcome to XO Game");
    }

    public static void showTurn() {
        System.out.println("Turn " + currentPlayer);
    }

    public static void inputRowCol() {

        System.out.println("Plaese input Row/Col :");
        row = sc.nextInt();
        col = sc.nextInt();
    }

    public static void process() {
        if (setTable()) {
            if (checkWin()) {
                finish = true;
                showWin();
                return;
            }
            if(checkDraw()){
                finish = true;
                showDraw();
                return;
            }
            count++;
            switchPlayer();
        }
    }

    public static void showWin() {
        showTable();
        System.out.println(">>>> " + currentPlayer + " Win <<<<");
    }

    public static void switchPlayer() {
        if (currentPlayer == 'O') {
            currentPlayer = 'X'; //switch player
        } else {
            currentPlayer = 'O';
        }
    }

    public static boolean setTable() {
        table[row - 1][col - 1] = currentPlayer;
        return true;
    }

    public static boolean checkWin() {
        if (checkVertical()) {
            return true;
        } else if (checkHorizontal()) {
            return true;
        } else if (checkX()) {
            return true;
        }
        return false;
    }

    public static boolean checkVertical() {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkHorizontal() {
        for (int c = 0; c < table.length; c++) {
            if (table[row - 1][c] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkX() {
        if (checkX1()) {
            return true;
        } else if (checkX2()) {
            return true;
        }
        return false;
    }

    private static boolean checkX1() { //11,22,33
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }

        }
        return true;
    }

    private static boolean checkX2() { //13,22,31
        for (int i = 0; i < table.length; i++) {
            if (table[i][2-i] != currentPlayer) {
                return false;
            }

        }
        return true;
    }

    public static boolean checkDraw() {
        if(count == 8){
            return true;
        }
        return false;
    }

    public static void showDraw() {
        System.out.println(">>>> Draw <<<<");
    }
}
